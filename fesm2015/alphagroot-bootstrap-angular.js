import * as i0 from '@angular/core';
import { Injectable, Component, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

class AlphagrootService {
    constructor() { }
}
AlphagrootService.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.0.4", ngImport: i0, type: AlphagrootService, deps: [], target: i0.ɵɵFactoryTarget.Injectable });
AlphagrootService.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "12.0.4", ngImport: i0, type: AlphagrootService, providedIn: 'root' });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.0.4", ngImport: i0, type: AlphagrootService, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], ctorParameters: function () { return []; } });

class AlphagrootComponent {
    constructor() { }
    ngOnInit() {
    }
}
AlphagrootComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.0.4", ngImport: i0, type: AlphagrootComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
AlphagrootComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.0.4", type: AlphagrootComponent, selector: "lib-alphagroot", ngImport: i0, template: `
    <p>
      alphagroot works!
    </p>
  `, isInline: true });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.0.4", ngImport: i0, type: AlphagrootComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'lib-alphagroot',
                    template: `
    <p>
      alphagroot works!
    </p>
  `,
                    styles: []
                }]
        }], ctorParameters: function () { return []; } });

class AlphaAlert {
    /*{
      jsAlert : true / undefined <- Optional
      message : '' <- Mandatory
    }*/
    show(_r) {
        if (_r.jsAlert != undefined) {
            alert(_r.message);
            return;
        }
        alert('bablas');
    }
}
AlphaAlert.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.0.4", ngImport: i0, type: AlphaAlert, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
AlphaAlert.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "12.0.4", ngImport: i0, type: AlphaAlert, imports: [CommonModule] });
AlphaAlert.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "12.0.4", ngImport: i0, type: AlphaAlert, imports: [[
            CommonModule
        ]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.0.4", ngImport: i0, type: AlphaAlert, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [],
                    imports: [
                        CommonModule
                    ]
                }]
        }] });

class AlphagrootModule {
}
AlphagrootModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.0.4", ngImport: i0, type: AlphagrootModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
AlphagrootModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "12.0.4", ngImport: i0, type: AlphagrootModule, declarations: [AlphagrootComponent], exports: [AlphagrootComponent,
        AlphaAlert] });
AlphagrootModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "12.0.4", ngImport: i0, type: AlphagrootModule, imports: [[], AlphaAlert] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.0.4", ngImport: i0, type: AlphagrootModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [
                        AlphagrootComponent
                    ],
                    imports: [],
                    exports: [
                        AlphagrootComponent,
                        AlphaAlert
                    ]
                }]
        }] });

/*
 * Public API Surface of alphagroot
 */

/**
 * Generated bundle index. Do not edit.
 */

export { AlphaAlert, AlphagrootComponent, AlphagrootModule, AlphagrootService };
//# sourceMappingURL=alphagroot-bootstrap-angular.js.map
