import * as i0 from "@angular/core";
import * as i1 from "./alphagroot.component";
import * as i2 from "./alert/alert.module";
export declare class AlphagrootModule {
    static ɵfac: i0.ɵɵFactoryDeclaration<AlphagrootModule, never>;
    static ɵmod: i0.ɵɵNgModuleDeclaration<AlphagrootModule, [typeof i1.AlphagrootComponent], never, [typeof i1.AlphagrootComponent, typeof i2.AlphaAlert]>;
    static ɵinj: i0.ɵɵInjectorDeclaration<AlphagrootModule>;
}
