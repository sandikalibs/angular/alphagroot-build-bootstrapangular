# AlphaGroot

Welcome!!

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.0.4.<br />
it help you to create Web Base Project easier.

## Prerequisite

- Angular Version >= 12.0.0
- Bootstrap 4.0


## Implement

`npm install @alphagroot/bootstrap-angular `<br />
or <br />
`@alphagroot/bootstrap-angular@<specified version>`

## Feature & Use

**Alert**
<details>
<summary>How To User Alert</summary> <br />

implement in App Module <br />
`import { AlphaAlert } from '@alphagroot/bootstrap-angular';`
 ```
 imports: [
    BrowserModule,
    AppRoutingModule,
    AlphaAlert
  ],
```


implement in Angular Component

`import { AlphaAlert } from '@alphagroot/bootstrap-angular';`

and in Constructor

`constructor(_AlphaAlert : AlphaAlert) { }`

then


```
ngOnInit() {
    
    this._AlphaAlert.show({
        jsAlert : true,
        message : 'Hallo World'
    })
}
```


Alert Request


| Request JSON | Value | Type |
| ------ | ------ | ------ |
| jsAlert | true / undefined | Optional |
| message | string | Mandatory |

</details>





> By Sandika Gusti Prakasa, 2021







