import { NgModule } from '@angular/core';
import { AlphagrootComponent } from './alphagroot.component';
import { AlphaAlert } from './alert/alert.module';
import * as i0 from "@angular/core";
export class AlphagrootModule {
}
AlphagrootModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.0.4", ngImport: i0, type: AlphagrootModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
AlphagrootModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "12.0.4", ngImport: i0, type: AlphagrootModule, declarations: [AlphagrootComponent], exports: [AlphagrootComponent,
        AlphaAlert] });
AlphagrootModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "12.0.4", ngImport: i0, type: AlphagrootModule, imports: [[], AlphaAlert] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.0.4", ngImport: i0, type: AlphagrootModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [
                        AlphagrootComponent
                    ],
                    imports: [],
                    exports: [
                        AlphagrootComponent,
                        AlphaAlert
                    ]
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWxwaGFncm9vdC5tb2R1bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi9wcm9qZWN0cy9hbHBoYWdyb290L3NyYy9saWIvYWxwaGFncm9vdC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUM3RCxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0JBQXNCLENBQUE7O0FBY2pELE1BQU0sT0FBTyxnQkFBZ0I7OzZHQUFoQixnQkFBZ0I7OEdBQWhCLGdCQUFnQixpQkFUekIsbUJBQW1CLGFBS25CLG1CQUFtQjtRQUNuQixVQUFVOzhHQUdELGdCQUFnQixZQVBsQixFQUNSLEVBR0MsVUFBVTsyRkFHRCxnQkFBZ0I7a0JBWDVCLFFBQVE7bUJBQUM7b0JBQ1IsWUFBWSxFQUFFO3dCQUNaLG1CQUFtQjtxQkFDcEI7b0JBQ0QsT0FBTyxFQUFFLEVBQ1I7b0JBQ0QsT0FBTyxFQUFFO3dCQUNQLG1CQUFtQjt3QkFDbkIsVUFBVTtxQkFDWDtpQkFDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBBbHBoYWdyb290Q29tcG9uZW50IH0gZnJvbSAnLi9hbHBoYWdyb290LmNvbXBvbmVudCc7XG5pbXBvcnQgeyBBbHBoYUFsZXJ0IH0gZnJvbSAnLi9hbGVydC9hbGVydC5tb2R1bGUnXG5cblxuQE5nTW9kdWxlKHtcbiAgZGVjbGFyYXRpb25zOiBbXG4gICAgQWxwaGFncm9vdENvbXBvbmVudFxuICBdLFxuICBpbXBvcnRzOiBbXG4gIF0sXG4gIGV4cG9ydHM6IFtcbiAgICBBbHBoYWdyb290Q29tcG9uZW50LFxuICAgIEFscGhhQWxlcnRcbiAgXVxufSlcbmV4cG9ydCBjbGFzcyBBbHBoYWdyb290TW9kdWxlIHsgfVxuIl19