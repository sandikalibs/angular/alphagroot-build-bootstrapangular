(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/common')) :
    typeof define === 'function' && define.amd ? define('@alphagroot/bootstrap-angular', ['exports', '@angular/core', '@angular/common'], factory) :
    (global = typeof globalThis !== 'undefined' ? globalThis : global || self, factory((global.alphagroot = global.alphagroot || {}, global.alphagroot['bootstrap-angular'] = {}), global.ng.core, global.ng.common));
}(this, (function (exports, i0, common) { 'use strict';

    function _interopNamespace(e) {
        if (e && e.__esModule) return e;
        var n = Object.create(null);
        if (e) {
            Object.keys(e).forEach(function (k) {
                if (k !== 'default') {
                    var d = Object.getOwnPropertyDescriptor(e, k);
                    Object.defineProperty(n, k, d.get ? d : {
                        enumerable: true,
                        get: function () {
                            return e[k];
                        }
                    });
                }
            });
        }
        n['default'] = e;
        return Object.freeze(n);
    }

    var i0__namespace = /*#__PURE__*/_interopNamespace(i0);

    var AlphagrootService = /** @class */ (function () {
        function AlphagrootService() {
        }
        return AlphagrootService;
    }());
    AlphagrootService.ɵfac = i0__namespace.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.0.4", ngImport: i0__namespace, type: AlphagrootService, deps: [], target: i0__namespace.ɵɵFactoryTarget.Injectable });
    AlphagrootService.ɵprov = i0__namespace.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "12.0.4", ngImport: i0__namespace, type: AlphagrootService, providedIn: 'root' });
    i0__namespace.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.0.4", ngImport: i0__namespace, type: AlphagrootService, decorators: [{
                type: i0.Injectable,
                args: [{
                        providedIn: 'root'
                    }]
            }], ctorParameters: function () { return []; } });

    var AlphagrootComponent = /** @class */ (function () {
        function AlphagrootComponent() {
        }
        AlphagrootComponent.prototype.ngOnInit = function () {
        };
        return AlphagrootComponent;
    }());
    AlphagrootComponent.ɵfac = i0__namespace.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.0.4", ngImport: i0__namespace, type: AlphagrootComponent, deps: [], target: i0__namespace.ɵɵFactoryTarget.Component });
    AlphagrootComponent.ɵcmp = i0__namespace.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "12.0.4", type: AlphagrootComponent, selector: "lib-alphagroot", ngImport: i0__namespace, template: "\n    <p>\n      alphagroot works!\n    </p>\n  ", isInline: true });
    i0__namespace.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.0.4", ngImport: i0__namespace, type: AlphagrootComponent, decorators: [{
                type: i0.Component,
                args: [{
                        selector: 'lib-alphagroot',
                        template: "\n    <p>\n      alphagroot works!\n    </p>\n  ",
                        styles: []
                    }]
            }], ctorParameters: function () { return []; } });

    var AlphaAlert = /** @class */ (function () {
        function AlphaAlert() {
        }
        /*{
          jsAlert : true / undefined <- Optional
          message : '' <- Mandatory
        }*/
        AlphaAlert.prototype.show = function (_r) {
            if (_r.jsAlert != undefined) {
                alert(_r.message);
                return;
            }
            alert('bablas');
        };
        return AlphaAlert;
    }());
    AlphaAlert.ɵfac = i0__namespace.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.0.4", ngImport: i0__namespace, type: AlphaAlert, deps: [], target: i0__namespace.ɵɵFactoryTarget.NgModule });
    AlphaAlert.ɵmod = i0__namespace.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "12.0.4", ngImport: i0__namespace, type: AlphaAlert, imports: [common.CommonModule] });
    AlphaAlert.ɵinj = i0__namespace.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "12.0.4", ngImport: i0__namespace, type: AlphaAlert, imports: [[
                common.CommonModule
            ]] });
    i0__namespace.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.0.4", ngImport: i0__namespace, type: AlphaAlert, decorators: [{
                type: i0.NgModule,
                args: [{
                        declarations: [],
                        imports: [
                            common.CommonModule
                        ]
                    }]
            }] });

    var AlphagrootModule = /** @class */ (function () {
        function AlphagrootModule() {
        }
        return AlphagrootModule;
    }());
    AlphagrootModule.ɵfac = i0__namespace.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "12.0.4", ngImport: i0__namespace, type: AlphagrootModule, deps: [], target: i0__namespace.ɵɵFactoryTarget.NgModule });
    AlphagrootModule.ɵmod = i0__namespace.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "12.0.4", ngImport: i0__namespace, type: AlphagrootModule, declarations: [AlphagrootComponent], exports: [AlphagrootComponent,
            AlphaAlert] });
    AlphagrootModule.ɵinj = i0__namespace.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "12.0.4", ngImport: i0__namespace, type: AlphagrootModule, imports: [[], AlphaAlert] });
    i0__namespace.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "12.0.4", ngImport: i0__namespace, type: AlphagrootModule, decorators: [{
                type: i0.NgModule,
                args: [{
                        declarations: [
                            AlphagrootComponent
                        ],
                        imports: [],
                        exports: [
                            AlphagrootComponent,
                            AlphaAlert
                        ]
                    }]
            }] });

    /*
     * Public API Surface of alphagroot
     */

    /**
     * Generated bundle index. Do not edit.
     */

    exports.AlphaAlert = AlphaAlert;
    exports.AlphagrootComponent = AlphagrootComponent;
    exports.AlphagrootModule = AlphagrootModule;
    exports.AlphagrootService = AlphagrootService;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=alphagroot-bootstrap-angular.umd.js.map
